import request from '@/utils/request'

export function login(username, password) {
  return request({
    url: '/account/login',
    method: 'post',
    data: {
      username,
      password
    }
  })
}

export function register(username, password, captcha) {
  return request({
    url: '/account/register',
    method: 'post',
    data: {
      username,
      password,
      captcha
    }
  })
}

export function getInfo() {
  return request({
    url: '/account/userinfo',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/account/logout',
    method: 'post'
  })
}
